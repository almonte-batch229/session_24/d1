console.log("Nadine Lustre")

/*	
	ES6 Updates:

	ES6 is one of the latest versions of writing JavaScript and infact is one of the latest major update to JS.
*/

// [SECTION] VAriable Declaration
// let,const - are ES6 updates to update the standard of creating variables.

/*
	console.log(sampleLet)
	let smapleLet = "Sample" -> error
*/

// var - was the keyword used previously before ES6

/*
	console.log(varSample)
	var varSample = "Hoist Me Up!" -> undefined
*/	

// [SECTION] Exponent Operator

// Math.pow() allows us to get the result of the number raised to the given exponent
/*
	Syntax:
		Math.pow(base, exponent)
*/
let fivePowerOf3 = Math.pow(5,3)
console.log(fivePowerOf3)

// Exponent Operator (**) allows us to get the result of a number raised to a given exponent. It is used as an alternative to Math.pow()

let fivePowerOf2 = 5**2
console.log(fivePowerOf2)

// Square Root
let squareRootOf4 = 4**.5
console.log(squareRootOf4)

// [SECTION] Template Literals

// " " and ' ' are called string literals

let word1 = "Javascript"
let word2 = "Java"
let word3 = "is"
let word4 = "not"

/*	
	let sentence1 = word1 + " " + word3 + " " + word4 + " " + word2 + "."
	console.log(sentence1)
*/

// ' ' backticks allows us to create strings using '' and easily embed JS expressions

// ${} is used in template literals to embed JS expresions and variables. it is also called a placeholder.

let sentence1 = `${word1} ${word3} ${word4} ${word2}.`
console.log(sentence1)

let sentence2 = `${word2} is an OOP Language.`
console.log(sentence2)

// template literals can also be used to embed JS expressions
let sentence3 = `The sum of 15 and 25 is ${15 + 25}`
console.log(sentence3)

let user1 = {
	name: "Michael",
	position: "Manager",
	income: 90000,
	expenses: 50000
}

console.log(`${user1.name} is a ${user1.position}`)
console.log(`His income is ${user1.income} and expenses at ${user1.expenses}. His current balance is ${user1.income - user1.expenses}`)

// [SECTION] Destructuring Arrays and Objects
// destructuring will allow us to save array elements or object properties into new variables without having to create/initialize with accessing items/properties one by one

let array1 = ["Curry", "Lilliard", "Paul", "Irving"]

/*
	let player1 = array1[0] // Curry
	let player2 = array1[1] // Lilliard
	let player3 = array1[2] // Paul
	let player4 = array1[3] // Irving
*/

// console.log(player1, player2, player3, player4)

// Array Destructuring is when you save array items into variables
// In arrays, order matters and that goes the same for destructuring
let [player1, player2, player3, player4] = array1
console.log(player1)
console.log(player4)

let array2 = ["Jokic", "Embiid", "Anthony-Towns", "Davis"]

let [center1,,,center2] = array2;
console.log(center1)
console.log(center2)

// Object Destructuring aloows us to get the value of a property and save in a variable of the same name.
// what matters are the keys/property names.
// the variable name should excatly match the property name.
let pokemon1 = {
	name: "Balbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf", "Tackle", "Leech Seed"]
}

let {type, level, name, moves, personality} = pokemon1

console.log(type)
console.log(level)
console.log(moves) 
console.log(name)
console.log(personality) // undefined

// Destructuring in a function
function greet(object) {
	/*
		When passed user1, object now contains the key-value pairs of user1
		object = {
			name: "Michael",
			position: "Manager",
			income: 90000,
			expenses: 50000
		}
	*/

	let {name} = object // allowed us to get object.name and save it as name

	console.log(`Hello ${name}`)
	console.log(`${name} is my frined!`)
	console.log(`Good Luck, ${name}`)
}

greet(user1)

// [Mini Activity]

let product1 = {
	productName: "Safeguard Handsoap",
	description: "Liquid Handsoap by Safeguard.",
	price: 25,
	isActive: true
}

let {productName, price} = product1

console.log(`The product name is ${productName}.`)
console.log(`The product price is ${price}.`)

// [SECTION] Arraow Functions
// It is an alternative way of writing functions in JS. However, there are significant pros and cons between traditional and arrow function

// tradition function
function displayMsg() {
	console.log("Hello World!")
} 

displayMsg()

// arrow function
const hello = () => {
	console.log(`Hello, Arrow!`)
}

hello()

// arrow functions with parameters
const alertUser = (username) => {
	console.log(`This is an alert for user ${username}`)
}

alertUser("James1991")

// we don't usually use the "let" keyword to assign our arrow function to avoid updating variable
// alertUser = "Hello, Pogi sir Rome"
// alertUser("Romenick100")

// arrow and traditional functions are pretty much the same. they are gunctions, however, there are some key differences.

// Implicit Return is the ability of an arrow function to return value without the use of return keyword.

// traditional addNum() function

function addNum(num1, num2) {
	let result = num1 + num2
	// result -> undefined
	return result
}

let sum1 = addNum(5,10)
console.log(sum1)

// arrow function have implicit return. When an arrow function is written in one line, it can return value without return keyword.

const addNumArrow = (num1, num2) => num1 + num2

let sum2 = addNumArrow(5,10)
console.log(sum2)

// Implicit return will only work on an arrow function written in one line and without {}
// if an arrow function is written in more than one line and with a {} then, we will need a return keyword

/*
	const subNum = (num1, num2) => {
		// num1 - num2 -> undefined
		return num1 - num2
	}
*/

const subNum = (num1, num2) => num1 - num2

let difference = subNum(20,10)
console.log(difference)

// traditional function vs arrow function as object methods

let character1 = {
	name: "Cloud Strife",
	occupation: "SOLDIER",
	introduceName: function() {
		// In a traditional function as a method, this refers to the object where the method is.
		console.log(`Hi! I am ${this.name}`)
	},
	introduceJob: () => {
		// In an arrow function as a method, this actually refers to the global window object document.
		// console.log(`My job is ${this.occupation}`)
		console.log(this)
	}
}

character1.introduceName()
character1.introduceJob()

const sampleObj = {
	name: "Smaple1",
	age: 25
}

// sampleObj = "Hello"

sampleObj.name = "Smith"

console.log(sampleObj)

// [SECTION] Class Based Object Blueprint
// In javascript, classes are templates of objects 
// we can use classes to create objects following the structure of the class similar to a constructor function

// constructor function
/*
	function Pokemon(name, type, level) {
		this.name = name
		this.type = type
		this.level = level
	}

	let pokemon2 = new Pokemon("Pikachu", "Electric", 25)
	console.log(pokemon2)
*/

// with the advent of ES6, we are now introduced to a new way of crating objects with a blueprint with the use of classes

class Car {
	constructor(brand, model, year) {
		this.brand = brand
		this.model = model
		this.year = year
	}
}

let car1 = new Car("Toyota", "Vios", "2002")
let car2 = new Car("Cooper", "Mini", "1967")
let car3 = new Car("Porsche", "911", "1968")

console.log(car1)
console.log(car2)
console.log(car3)

// Mini Activity

class Pokemon {
	constructor(name, type, level) {
		this.name = name
		this.type = type
		this.level = level
	}
}

let pokemonCharacter1 = new Pokemon("Pikachu", "Electric", 25)
let pokemonCharacter2 = new Pokemon("Balabsaur", "Grass", 30) 

console.log(pokemonCharacter1)
console.log(pokemonCharacter2)

// [SECTION] Arrow Functions in Array Methods
 
let numArr = [2, 10, 3, 10, 5]

// array method with traditional Functions
/*
	let reduceNumber = numArr.reduce(function(x,y) {
		// get the sum of all numbers in the array
		return x + y
	})	
*/

// array method with arrow function
/*
	let reduceNumber = numArr.reduce((x,y) => {
		return x + y
	})
*/

// reduced with implicit return
let reduceNumber = numArr.reduce((x,y) => x + y)

console.log(reduceNumber)

// traditional function
/*
	let mappedNum = numArr.map(function(num) {
		return num * 2
	})
*/

// arrow function
let mappedNum = numArr.map((num) => {
	return num * 2
})

console.log(mappedNum)